#!/bin/bash
set -o errexit
set -o nounset

SCRIPTDIR=$(realpath $(dirname $0))
cd $SCRIPTDIR

if [ "$EUID" -ne 0 ]
then echo "trust me ;) run as root"
  exit
fi


echo Note this should be run from an Ubuntu build on the agent you want to pxe boot.
echo Go ahead? [Y/n]
read go
if ! [[ $go =~ ^[yY][eE]*[sS]*$ ]]
 then
  echo quitting
  exit
fi


#ensure up to date
sudo apt -qy update && sudo apt -qy full-upgrade

echo Kernel last updated around $(cat /var/log/apt/history.log   |grep kernel -B4 -A1  | tail -6 | awk '/Start-Date: ([0-9]{4}-[0-9]{2}-[0-9]{2})/ {print $2}')
echo Reboot? Do this if apt has just updated the kernel...
echo Reboot? [Y/n]
read go
if [[ $go =~ ^[yY][eE]*[sS]*$ ]]
 then
  reboot
fi


##### maybe check if there's an updated release
#do-release-upgrade

#install pi specific stuff
apt -qy install libraspberrypi-bin rpi-eeprom

#prevent future auto eeprom updates
systemctl mask rpi-eeprom-update
#set boot order - network then sd
rpi-eeprom-config > bootconfig.txt
sed -i '/BOOT_ORDER/d' bootconfig.txt
echo "BOOT_ORDER=0xf12" >> bootconfig.txt
rpi-eeprom-config --apply bootconfig.txt
rm bootconfig.txt

#first firmware to support pxe boot was 2020-09-03
#if you need to pick one:
#eg
#rpi-eeprom-update -d -f /lib/firmware/raspberrypi/bootloader/critical/pieeprom-2020-09-03.bin



export SERIAL=$(vcgencmd otp_dump | grep 28: | sed s/.*://g)
export MAC=$(ip addr | grep -A1 eth0 | grep ether | awk '{print $2}')

echo "Serial $SERIAL"
echo "MAC    $MAC"



