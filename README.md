# Raspberry Pi Cluster
## Getting Started
### What you need
Quantity | Product
--- | ---
At least 2x | Raspberry Pi 4 (RAM dependant on your plans)
1x | USB 3 M.2 NVMe and SATA SSD Enclosure Adapter
1x | M.2 PCIe NVMe SSD (size dependant on your plans)
1x | SD Card
1x | Ethernet Switch (1GB preferable)
1x | USB wall charger (at least 3 amps per port simultaneously)
1-per-Pi | USB Cables (whatever the charger outputs -> USB C)
1-per-Pi | Ethernet cable
(optional) | Cluster Case
### Configure
1. (once) Prepare the NVMe Drive
   - Get the [Raspberry Pi Imager](https://www.raspberrypi.com/software/)
   - Chose 64 bit Ubuntu Server 21.10 (or later) and write to NVMe drive
   - Resize and partition now if you plan to
     - Before booting, connect drive to a running Pi

           sfdisk /dev/sda < server/sda.sfdisk

       - OPTIONAL To create this map, use cfdisk to go through a text gui
if you get "Partition [n] does not start on physical sector boundary."
you can manually tweak them ensuring sectors are multiles of 64

         - write map:

           sfdisk -d /dev/sda > sda.sfdisk


   - Might also be a good time to setup networking so you don't need a screen
     - Mount the main disk and grab the template from ./server/50-cloud-init.yaml

           mkdir -p /mnt/tmp ; mount /dev/sda2 /mnt/tmp
           vi /mnt/tmp/etc/netplan/50-cloud-init.yaml
           cp server/99-disable-network-config.cfg.erb /mnt/tmp/etc/cloud/cloud.cfg.d/99-disable-network-config.cfg

2. (once) Prepare the primary Pi
   - Flash Raspberry Pi OS or Ubuntu to the SD card with [Raspberry Pi Imager](https://www.raspberrypi.com/software/), connect the SD card and USB NVMe drive to the Pi and boot it.
     - Export current boot config (if this doesn't work, install the package first)

           sudo apt install rpi-eeprom
           rpi-eeprom-config > bootconfig.txt

     - Edit BOOT_ORDER, add 4 for USB boot

           BOOT_ORDER=0xf14

     - Write new config and reboot

           rpi-eeprom-config --apply bootconfig.txt
           rm bootconfig.txt
           shutdown -h now

3. (once) Login and do initial setup of control server
     - Once connected clone this repo and run server
       You can do this by connecting a screen and keyboard, or temporarily connecting ethernet to your main router.
       - ssh to your new Pi (or use keyboard/screen). To find your ip address, checkout the network section of your router's admin page.
       - Username: ubuntu. Password: ubuntu.
       - Clone this repo, configure and run server-config.sh. To configure, you just need to fill in MAC and serial numbers for your agents.
       
             ssh-keygen
             mkdir ~/src
             cd ~/src
             git clone git@gitlab.com:merob/picluster.git
             cd picluster/
             cp setenv.sh.example setenv.sh
             vi setenv.sh #OR nano, emacs, or your favourite text editor
             cd server
             sudo ./server-config.sh

     - Format and mount os partition

             sudo -s
             mkfs.xfs /dev/sda3
             mkdir /agentos
             echo "$(blkid /dev/sda3 | awk '{print $2}') /agentos xfs defaults 1 1" >> /etc/fstab
             mount -a

4. Build agent disks

             ./prep-nfs-tftp.sh 
             #dnsmasq.conf.erb read error on first run. Don't know why. Run again...
             ./prep-nfs-tftp.sh
             
