#!/bin/bash
set -o errexit
set -o nounset

function patchfile(){
  if ! patch --force --reverse --dry-run $1 $2 >/dev/null
    then 
      if ! patch --force $1 $2
        then
          echo "Patch failed"
      fi
  fi
}

SCRIPTDIR=$(realpath $(dirname $0))
cd $SCRIPTDIR

if [ "$EUID" -ne 0 ]
then echo "trust me ;) run as root"
  exit
fi

if [[ -f ../setenv.sh ]]
 then
  source ../setenv.sh
fi

QUIT=no

echo "About to install some things, fix some config. Go ahead?"
echo "[Y/n]"
read go
if ! [[ $go =~ ^[yY][eE]*[sS]*$ ]]
 then
  QUIT="yes"
fi

if [[ $QUIT == "yes" ]]
 then
  echo "Quittin' time"
  exit
fi

apt update
apt upgrade -y
apt -qy install nfs-kernel-server dnsmasq iptables-persistent

#useful but not essential
apt -qy install net-tools

#backup originals
TODAY=$(date +%Y-%m-%d)
for FILE in /usr/lib/systemd/system/dnsmasq\@.service \
	    /etc/sysctl.conf \
	    /etc/netplan/10-rpi-ethernet-eth0.yaml \
	    /etc/netplan/50-cloud-init.yaml \
	    /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg \
            /etc/exports
  do
    if [[ ! -f ${FILE}.${TODAY}.orig ]]
      then
        if ! cp $FILE ${FILE}.${TODAY}.orig
          then
	    echo "$FILE doesn't exist. Not backing up"
        fi
    else
      echo "backup ${FILE}.${TODAY}.orig already exists. Not backing up again" 
    fi
done


patchfile /usr/lib/systemd/system/dnsmasq\@.service dnsmasq\@.service.patch
patchfile /etc/sysctl.conf sysctl.conf.patch

sed "s/<%= @NFSSERVER %>/$NFSSERVER/g" 10-rpi-ethernet-eth0.yaml.erb > /etc/netplan/10-rpi-ethernet-eth0.yaml
sed "s/<%= @WIFI_SSID %>/$WIFI_SSID/g" 50-cloud-init.yaml.erb | sed "s/<%= @WIFI_PASSWORD %>/$WIFI_PASSWORD/g" > /etc/netplan/50-cloud-init.yaml
cp 99-disable-network-config.cfg.erb /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg

netplan generate
netplan apply

iptables -A FORWARD -i eth0 -j ACCEPT
iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
iptables-save > /etc/iptables/rules.v4
