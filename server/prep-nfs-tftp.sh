#!/bin/bash
set -o errexit
set -o nounset

SCRIPTDIR=$(realpath $(dirname $0))
cd $SCRIPTDIR

if [ "$EUID" -ne 0 ]
then echo "trust me ;) run as root"
  exit
fi

if [[ -f ../setenv.sh ]]
 then
  source ../setenv.sh
fi

QUIT=no
if [[ ${PUBKEY-unset} == "unset" ]] ; then
  echo "INFO. Using default public key location"
  PUBKEY="$HOME/.ssh/id_rsa.pub"
fi  
if [[ ${ETH-unset} == "unset" ]] ; then
  echo "WARNING. Using default eth device"
  ETH="eth0"
fi  
if [[ ${SUBNET24-unset} == "unset" ]] ; then
  echo "WARNING. Using default agent subnet"
  SUBNET24="10.3.14"
fi  
if [[ ${NFSSERVER-unset} == "unset" ]] ; then
  echo "WARNING. Using default nfs server"
  NFSSERVER="10.3.14.1"
fi  
if [[ ${macs-unset} == "unset" ]] ; then
  echo "ERROR. Need to define mac addresses of agents. Eg: "
  echo ' export macs=("01:23:45:67:89:ab") #separate with spaces'
  QUIT="yes"
fi  
if [[ ${serials-unset} == "unset" ]] ; then
  echo "ERROR. Need to define serial addresses of agents. Eg: "
  echo ' export serials=("ab123456")'
  QUIT="yes"
fi  

if ! [[ -f $PUBKEY ]]
 then
  echo "No PKI. Want me to try to create one?"
  echo "[Y/n]"
  read go
  if ! [[ $go =~ ^[yY][eE]*[sS]*$ ]]
   then
    QUIT="yes"
  else
    ssh-keygen
  fi
fi

echo
echo "You are using NIC ${ETH} for your cluster comms"
echo
echo "you are using ${SUBNET24}.0/24 as your cluster subnet"
echo
echo "these files will change:"
echo "/etc/dnsmasq.conf"
echo "/etc/exports"
echo "/etc/fstab"
echo
echo Go ahead? [Y/n]
read go
if ! [[ $go =~ ^[yY][eE]*[sS]*$ ]]
 then
  QUIT="yes"
fi

if [[ $QUIT == "yes" ]]
 then
  echo "Quittin' time"
  exit
fi


if ! mount | grep u2110 
 then
  echo "Trying to mount Ubuntu 2110:"
  echo
  if ! [[ -f /home/ubuntu/ubuntu-21.10-preinstalled-server-arm64+raspi.img ]]
   then
    cd /home/ubuntu ; curl https://cdimage.ubuntu.com/releases/21.10/release/ubuntu-21.10-preinstalled-server-arm64+raspi.img.xz -LO
    unxz ubuntu-21.10-preinstalled-server-arm64+raspi.img.xz
    cd $(dirname $0)
  fi
  mountpoint=boot
  for device in $(kpartx -va /home/ubuntu/ubuntu-21.10-preinstalled-server-arm64+raspi.img  | awk '{print $3}')
   do
    mkdir -p /mnt/u2110_${mountpoint}
    MOUNT="mount -o loop /dev/mapper/${device} /mnt/u2110_${mountpoint}"
    if ! eval $MOUNT
     then
      echo "failed to mount ${mountpoint}. HELP! Maybe try something like this:"
      echo "sudo $MOUNT"
      exit
    fi
    if [[ "$mountpoint" == "root" ]] 
     then
      break
    fi
    mountpoint=root
  done
fi

sed "s/<%= @SUBNET24 %>/$SUBNET24/g" dnsmasq.conf.erb > /etc/dnsmasq.conf

IP=101
for MAC in "${macs[@]}"
 do 
  #or use this to dynamically assign:   dhcp-host=${MAC},set:boot
  echo "dhcp-host=${MAC},set:boot,${SUBNET24}.${IP}" >> /etc/dnsmasq.conf
  IP=$(($IP+1))
done

cat exports.erb > /etc/exports

for SERIAL in "${serials[@]}"
 do
  mkdir -p /tftpboot/$SERIAL
  NEWMOUNT="/agentos/$SERIAL/boot /tftpboot/$SERIAL none defaults,bind 0 0"
  rm -rf /agentos/$SERIAL
  mkdir -p /agentos/$SERIAL
  echo "/agentos/$SERIAL *(rw,sync,no_subtree_check,no_root_squash)" | sudo tee -a /etc/exports
  rsync -xa --progress /mnt/u2110_root/ /agentos/$SERIAL/

  if mountpoint -q /tftpboot/$SERIAL
    then
      umount /tftpboot/$SERIAL
  fi
  rm -rf /agentos/$SERIAL/boot
  mkdir -p /agentos/$SERIAL/boot
  cp -r /boot/* /agentos/$SERIAL/boot
  if cd /agentos/$SERIAL/boot
   then
    ln -s dtbs/$(uname -r)/bcm2711-rpi-4-b.dtb .
    ln -s firmware/fixup4.dat .
    ln -s firmware/start4.elf .
    ln -s firmware/start4cd.elf .
    cp vmlinuz-$(uname -r) vmlinuz-$(uname -r).old
    zcat vmlinuz-$(uname -r).old > vmlinuz-$(uname -r)
    chmod go+r vmlinuz-$(uname -r)

  else
    echo FAILED
    exit
  fi
  if ! grep "$NEWMOUNT" /etc/fstab
   then
    echo $NEWMOUNT >> /etc/fstab
  fi
  if ! mountpoint -q /tftpboot/$SERIAL
    then
      mount /tftpboot/$SERIAL
  fi
  cd $SCRIPTDIR
  sed "s/<%= @NFSSERVER %>/$NFSSERVER/g" cmdline.txt.erb | sed "s/<%= @SERIAL %>/$SERIAL/g" > /agentos/$SERIAL/boot/cmdline.txt
  sed "s/<%= @KERNEL %>/$(uname -r)/g" config.txt.erb > /agentos/$SERIAL/boot/config.txt

  sed -i s/raspberrypi/$SERIAL/g /agentos/$SERIAL/etc/hostname
  sed -i s/raspberrypi/$SERIAL/g /agentos/$SERIAL/etc/hosts
  sed "s/<%= @NFSSERVER %>/$NFSSERVER/g" fstab.erb | sed "s/<%= @SERIAL %>/$SERIAL/g" > /agentos/$SERIAL/etc/fstab
  mkdir -p /agentos/$SERIAL/home/ubuntu/.ssh
  cat $PUBKEY > /agentos/$SERIAL/home/ubuntu/.ssh/authorized_keys
  chmod 0644 /agentos/$SERIAL/home/ubuntu/.ssh/authorized_keys
  sed -i '/PasswordAuthentication yes/d' /agentos/$SERIAL/etc/ssh/sshd_config
  echo "PasswordAuthentication=no" >> /agentos/$SERIAL/etc/ssh/sshd_config
done


systemctl stop systemd-resolved
sudo systemctl disable systemd-resolved
systemctl daemon-reload
systemctl restart dnsmasq
systemctl enable dnsmasq

systemctl restart nfs-server
